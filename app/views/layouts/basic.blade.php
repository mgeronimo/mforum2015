<!DOCTYPE html>
<!--
  BASIC LAYOUT
  - This is basically the layout used for the whole website. In this file, all the parts of the website which are repetitively used are incorporated in order to prevent the need of coding it again and again.
  - This includes almost all the parts of the website except for the parts that needs to be changed all the time. The parts included here are the header, the very format of the page, footer, and side menu.
  - Part that can be changed has the "@yield('')" code (i.e. content of the page).
-->
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="assets/jquery-mobile/jquery.mobile.structure-1.3.1.css" />
  <link rel="stylesheet" href="css/font-awesome.css" />
  <link rel="stylesheet" href="font/stylesheet.css" />
  <link rel="stylesheet" href="assets/flex-slider/flexslider.css" type="text/css">

  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/custom.css" />

  <script src="js/jquery-1.9.1.min.js"></script>
  <script src="js/jquery.mobile-1.3.0.min.js"></script>
  <script src="assets/flex-slider/jquery.flexslider-min.js"></script>
  <script src="js/main.js"></script>
</head>
<body >
  <div data-role="page" data-theme="a">
    <!-- Side Navigation Panel -->
    <div data-role="panel" id="sideNav" data-position="left">
      <ul id="sideButtons">
        <li>
          <a href="https://www.facebook.com/cohredforum" target="_blank"><i class="icon-facebook"></i></a>
        </li>
        <li>
          <a href="https://twitter.com/COHRED_Forum" target="_blank"><i class="icon-twitter"></i></a>
        </li>
      </ul>
      <ul id="sidePanel" class="clleft">
        <!-- If-else statements in this section serves as a checker of the current URL in order to set the active menu item properly -->
        <li @if((Request::path() == '/') || (Request::path() == 'important-dates') || (Request::path() == 'about-forum-2015')) id="active" @endif><a href="/">&nbsp; Home</a></li>
        <li @if((Request::path() == 'about-2015') || (Request::path() == 'about-2015/background-and-objectives') || (Request::path() == 'about-2015/cohred') || (Request::path() == 'about-2015/dost') || (Request::path() == 'about-2015/doh') || (Request::path() == 'about-2015/pchrd') || (Request::path() == 'about-2015/health-research-in-the-phil')) id="active" @endif>
          <a href="../about-2015">&nbsp; About 2015</a>
        </li>
        <li @if((Request::path() == 'get-involved') || (Request::path() == 'get-involved/register') || (Request::path() == 'get-involved/submit-an-abstract') || (Request::path() == 'get-involved/sponsorship-and-marketplace')) id="active" @endif>
          <a href="../get-involved">&nbsp; Get Involved</a>
        </li>
        <li @if(Request::path() == 'programme-at-a-glance') id="active" @endif>
          <a href="../programme-at-a-glance">&nbsp; Programme</a>
        </li>
        <li @if((Request::path() == 'practical-information') || (Request::path() == 'why-attend-forum-2015') || (Request::path() == 'practical-information/about-manila-and-philippines') || (Request::path() == 'practical-information/accomodation') || (Request::path() == 'practical-information/travel-visa') || (Request::path() == 'practical-information/picc-venue') || (Request::path() == 'practical-information/tours-and-excursion') || (Request::path() == 'practical-information/faq')) id="active" @endif>
          <a href="../practical-information">&nbsp; Practical Information</a>
        </li>
        <li @if(Request::path() == 'news') id="active" @endif>
          <a href="../news">&nbsp; News</a>
        </li>
        <li @if(Request::path() == 'sponsors') id="active" @endif>
          <a href="../sponsors">&nbsp; Sponsors</a>
        </li>
      </ul>
    </div>
    <!-- End Side Navigation -->

    <!-- Header -->
    <div data-role="header" data-position="fixed">
      <a href="#sideNav" id="navIcon"><i class="icon-th-list"></i></a> <!-- Menu icon -->
      <!-- Header icons -->
      <div id="header-icons">
        <a href="https://twitter.com/COHRED_Forum" class="h-icons" target="_blank"><span><i class="icon-twitter"></i></span></a>
        <a href="downloads" class="h-icons" style="font-size: 22px;"><span><i class="icon-download"></i></span></a>
        <a href="contact-us" class="h-icons" style="font-size: 22px;"><span style="position: relative; top: 1px;"><i class="icon-phone"></i></span></a>
      </div>
      @if (Request::path() != '/')
          <a href="javascript:history.back()" id="backButton" class="ui-btn-right"><i class="icon-angle-left"></i> &nbsp;<span class="ui-btn-text">Back</span></a>
      @endif
    </div>
    <!-- End Header -->


    <!-- Content Start -->
    <div data-role="content">
      <a href="/"><img src="../images/logo-2.png" id="logo" /></a>
      @yield('content')

      <div class="footer text-center">
        <p id="hosts">
          <a href="http://www.doh.gov.ph/" target="_blank"><img src="../images/hosts/doh.jpg"/></a>
          <a href="http://www.dost.gov.ph/" target="_blank"><img src="../images/hosts/dost.jpg"/></a>
          <a href="http://pchrd.dost.gov.ph/" target="_blank"><img src="../images/hosts/pchrd.jpg"/></a>
          <a href="http://www.cohred.org/" target="_blank"><img src="../images/hosts/cohred.jpg"/></a>
        </p>
        <p id="copyright">COPYRIGHT 2014. ALL RIGHTS RESERVED.<br/>POWERED BY <a href="http://solutionsresource.com/" target="_blank">SOLUTIONS RESOURCE, INC.</a>.</p>
      </div><!-- /footer -->
    </div>
    <!-- End Content -->
  

  </div><!-- /page -->

</body>
</html>