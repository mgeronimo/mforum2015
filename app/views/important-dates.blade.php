@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">IMPORTANT DATES</h1>
	<div class="page-content">
        <p class="home-preview impt-dates center margin-b"><strong>30 January 2015</strong><br/>Abstract submission opens</p>
        <p class="home-preview impt-dates center margin-b"><strong>30 March 2015</strong><br/>Call for session deadline</p>
        <p class="home-preview impt-dates center margin-b"><strong>31 March 2015</strong><br/>Abstract submission deadline</p>
        <p class="home-preview impt-dates center margin-b"><strong>June 2015</strong><br/>Abstract notification letter</p>
	</div>
@stop