@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">WHY ATTEND FORUM 2015?</h1>
	<div class="page-content">
		<ul class="home-preview">
		  <li>To <strong>learn</strong> and <strong>identify</strong> new solutions to the world’s unmet health needs</li>
		  <li>To <strong>engage</strong> with influential actors and leaders in research and innovation for health (e.g. senior government officials; senior academics and leading researchers; business and industry leaders; NGOs and CSOs; social entrepreneurs and other health care professionals)</li>
		  <li>To <strong>showcase</strong> health research and innovation on a global platform</li>
		  <li>To <strong>reach</strong> a new global audience by leveraging pre-event, live-event and post-event media opportunities</li>
		  <li>To <strong>network</strong> and <strong>develop</strong> partnerships across a spectrum of different industries and sectors including health, food, agriculture, environment, finance, infrastructure, etc.</li>
		  <li>To <strong>take part</strong> in one of the largest events on health in Southeast Asia in conjunction with the launch of the ASEAN economic community and the 2015 APEC summit</li>
		  <li>To <strong>gain</strong> insight into present and future of research and innovation in Southeast Asia</li>
		  <li>To actively <strong>engage</strong> with low and middle income countries as they set their own agendas for health research and innovation</li>
		  <li>To <strong>engage</strong> young leaders and innovators of tomorrow</li>
		  <li>To help <strong>strengthen</strong> people-centered healthcare by harmonizing mind and body, people and systems within the health sector and between health and other sectors as the Forum highlights People at the Center of Research and Innovation for Health.</li>
		</ul>
	</div>
@stop