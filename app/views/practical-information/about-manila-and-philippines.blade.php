@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">About Manila & Philippines</h1>
	<div class="page-content">
        <p id="reference">Reference:<br/>Tourism Promotions Board Philippines (<a href="http://www.tpb.gov.ph/">www.tpb.gov.ph</a>)<br/>
            Department of Tourism (<a href="http://www.tourism.gov.ph/">www.tourism.gov.ph</a>)
		<h3 class="font-roboto">Philippines</h3>
    	<p class="home-preview">The Philippines is an irresistible package of old-charm and modernity; with its world-class facilities; its intriguing mix of cultural traditions and one of the most hospitable people on earth. This festive archipelago of 7,107 delightful islands and 96 million fun-loving Filipinos is a rewarding experience.</p>
        <p class="home-preview">The archipelago is a charming blend of east and west, a gentle Asian nation spiced with Spanish pageantry and American verve. Its culture and cuisine reflect this delightful mélange of influences. It is also the third largest English-speaking country in the world, while Spanish is still spoken fluently by a small segment of the population.</p>
    	<p class="home-preview">The number and variety of its natural attractions, combined with deluxe accommodations, plush restaurants, modern shopping centers and, most of all, its hospitable people make the Philippines an incredibly versatile and colorful meetings and incentives destination.</p>
    	<h3 class="font-roboto">Metro Manila</h3>
    	<p class="home-preview">Metro Manila is an intriguing blend of the past and the present, of striking contrasts and contradictions. Blessed with significant historical and cultural destinations, it is likewise enriched with skyscraping edifices, world-class convention facilities, state of the art shopping malls and entertainment venues, and strips of restaurants that serve international gastronomic delights.</p>
    	<p class="home-preview">As a major gateway to the country’s 7,107 alluring islands, Metro Manila is the nation’s heart and soul. Believed to be the ultimate city to millions of Filipinos living outside the metropolis, Metro Manila is the central nerve of all activities in the country, the seat of government, the center of economic activities and the core of education, culture and the arts.</p>
		<p class="home-preview">It is a melting pot of various people, various cultures and various beliefs. Metropolitan Manila is the pulsating domain that will remain to be the stalwart realm of the nation.</p>
    	<p class="home-preview">For more information visit, Tourism Promotions Board Philippines at <a href="http://www.tpb.gov.ph/" target="_blank">www.tpb.gov.ph</a> and Department of Tourism at <a href="http://www.tpb.gov.ph/" target="_blank">www.tourism.gov.ph</a></p>
        <iframe src="//www.youtube.com/embed/ADNgEHFDYzo" frameborder="0" allowfullscreen style="width: 100%; height: 260px;"></iframe>
	</div>
@stop