@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">Tours and Excursion</h1>
	<div class="page-content">
		<p class="home-preview">Forum 2015's partner agency for the event, INTAS, based in Manila, manages all hotel reservations and tours & excursion.</p>
		<p class="home-preview">Please send an email to <a href="mailto:accommodation@forum2015.org" target="_blank">accommodation@forum2015.org</a> for tours & excursion inquiries. </p>
		
		<!--Manila city tour-->
		<h3 class="font-roboto">Manila City Tour</h3>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><strong>Date</strong></td>
				<td>22 August 2015 and 27 August 2015</td>
			</tr>
			<tr>
				<td><strong>Time</strong></td>
				<td>09:00 to 14:00</td>
			</tr>
			<tr>
				<td><strong>Price</strong></td>
				<td>$60.00</td>
			</tr>
			<tr>
				<td><strong>Minimum No. of Pax</strong></td>
				<td>30</td>
			</tr>
			<tr>
				<td><strong>What to bring</strong></td>
				<td>Sun care, comfortable shoes, hat.</td>
			</tr>
			<tr>
				<td><strong>What to wear</strong></td>
				<td>Comfortable, casual clothing. Skimpy clothing not recommended as there were will be church visits.</td>
			</tr>
			<tr>
				<td><strong>Inclusions</strong></td>
				<td>With transportation, English-speaking guide, entrance fees, lunch with a round of local beer or soda or mineral water</td>
			</tr>
		</table>
		<p class="home-preview">This tour will take you to Malacañang Museum and Library- learn the history of the official residence and principal workplace of the Chief Executive of the Philippines, Fort Santiago – the seat of the Spanish government in the 17th century. Visit The Manila Cathedral, Casa Manila a replica of a 17th century house, San Agustin Church and its museum. Tour will also include lunch in a local Filipino restaurant.</p>
		
		<!--Shopping tour-->
		<h3 class="font-roboto">Shopping Tour</h3>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><strong>Date</strong></td>
				<td>22 August 2015 and 28 August 2015</td>
			</tr>
			<tr>
				<td><strong>Time</strong></td>
				<td>14:00 to 19:00</td>
			</tr>
			<tr>
				<td><strong>Price</strong></td>
				<td>$30.00</td>
			</tr>
			<tr>
				<td><strong>Minimum No. of Pax</strong></td>
				<td>30</td>
			</tr>
			<tr>
				<td><strong>What to bring</strong></td>
				<td>Comfortable shoes</td>
			</tr>
			<tr>
				<td><strong>What to wear</strong></td>
				<td>Comfortable, casual clothing</td>
			</tr>
			<tr>
				<td><strong>Inclusions</strong></td>
				<td>With private transportation and English-speaking coordinator</td>
			</tr>
		</table>
		<p class="home-preview">You will be brought to the most fantastic shopping places in Manila where you need not think twice about buying any item. In Greenhills Shopping Mall, you can find all sorts of bargains. Greenhills combines upscale and mass market shops. One whole floor is devoted to mobile phones, while another is devoted to computer hardware, while another section is pearls galore.</p>

		<!--Tagaytay tour-->
		<h3 class="font-roboto">Tagaytay Tour</h3>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><strong>Date</strong></td>
				<td>23 August 2015</td>
			</tr>
			<tr>
				<td><strong>Time</strong></td>
				<td>09:00 to 15:00</td>
			</tr>
			<tr>
				<td><strong>Price</strong></td>
				<td>$105.00</td>
			</tr>
			<tr>
				<td><strong>Minimum No. of Pax</strong></td>
				<td>30</td>
			</tr>
			<tr>
				<td><strong>What to bring</strong></td>
				<td>Sun care, comfortable shoes, hat.</td>
			</tr>
			<tr>
				<td><strong>What to wear</strong></td>
				<td>Comfortable, casual clothing</td>
			</tr>
			<tr>
				<td><strong>Inclusions</strong></td>
				<td>With transportation, English-speaking guide, entrance fees, lunch with a round of local beer or soda or mineral water</td>
			</tr>
		</table>
		<p class="home-preview">Tagaytay, located on a ridge 2,250 ft. above sea level, is a leisurely drive to the countryside 60 kms. south of Manila, past small coastal villages and towns; rice fields, tropical fruit orchards and coconut plantation. From the heights of Tagaytay, enjoy the breathtaking sight of one of the world's strangest wonders-- Taal Volcano, a volcano within a volcano and lake within a lake.</p>

		<!--Manila Bay Cruise-->
		<h3 class="font-roboto">Manila Bay Cruise</h3>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><strong>Date</strong></td>
				<td>25 August 2015 and 27 August 2015</td>
			</tr>
			<tr>
				<td><strong>Time</strong></td>
				<td>17:00 to 19:00</td>
			</tr>
			<tr>
				<td><strong>Price</strong></td>
				<td>$50.00</td>
			</tr>
			<tr>
				<td><strong>Minimum No. of Pax</strong></td>
				<td>30</td>
			</tr>
			<tr>
				<td><strong>What to wear</strong></td>
				<td>Comfortable, casual clothing</td>
			</tr>
			<tr>
				<td><strong>Inclusions</strong></td>
				<td>With transportation, English-speaking guide, entrance fees, dinner with a round of local beer or soda or mineral water</td>
			</tr>
		</table>
		<p class="home-preview">Spending a pleasant tropical evening becomes more exhilarating with a sumptuous set dinner, over an hour of bay cruise and romantic serenade. As the sun goes down, indulge yourself to an enchanting experience ideal for those who want to have a quick getaway from the hustle and bustle of the city. Be awed with the Manila skyline by sunset and the lights of SM Mall of Asia by night as the cruise takes you across the Manila Bay for an hour of night time spectacle.</p>

		<!--Pagsanjn Falls Tour-->
		<h3 class="font-roboto">Pagsanjn Falls Tour</h3>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><strong>Date</strong></td>
				<td>30 August 2015</td>
			</tr>
			<tr>
				<td><strong>Time</strong></td>
				<td>08:00 to 16:00</td>
			</tr>
			<tr>
				<td><strong>Price</strong></td>
				<td>$.00</td>
			</tr>
			<tr>
				<td><strong>Minimum No. of Pax</strong></td>
				<td>30</td>
			</tr>
			<tr>
				<td><strong>What to bring</strong></td>
				<td>Sun care, slippers, swimwear, towel change of clothes as guests may get wet during the boat ride</td>
			</tr>
			<tr>
				<td><strong>What to wear</strong></td>
				<td>Comfortable clothing</td>
			</tr>
			<tr>
				<td><strong>Inclusions</strong></td>
				<td>With transportation, English-speaking guide, entrance fees, lunch with a round of local beer or soda or mineral water</td>
			</tr>
		</table>
		<p class="home-preview">Drive south of Manila to Pagsanjan, Laguna (approximately 2 hours) on an air -conditioned coach. The fun begins as you board your dugout canoes that will take you to the falls. "Shoot the rapids" with expert boatmen traversing the turbulent waters and tying to outdo each other in friendly, frenzied competition. Spend some time at the falls for the resort. </p>
	</div>
@stop