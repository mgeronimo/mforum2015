@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">PRACTICAL INFORMATION</h1>
    <div data-role="collapsible">
      <h1>WHY ATTEND FORUM 2015?</h1>
      <ul class="home-preview">
        <li>To <strong>learn</strong> about and <strong>identify</strong> new solutions to the world’s unmet health needs</li>
        <li>To <strong>engage</strong> with influential actors and leaders in research and innovation for health (e.g. senior government officials)</li>
      </ul>
      <p class="read-more"><a href="why-attend-forum-2015">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>ABOUT MANILA AND PHILIPPINES</h1>
      <h3 class="font-roboto">Philippines</h3>
      <p class="home-preview">The Philippines is an irresistible package of old-charm and modernity; with its world-class facilities; its intriguing mix of cultural traditions and one of the most hospitable people on earth.</p>
      <p class="read-more"><a href="../practical-information/about-manila-and-philippines">READ MORE</a></p>
    </div>
    <a href="../practical-information/accomodation" data-role="button" class="btn swatch-5 home-menu">ACCOMODATION</a>
    <div data-role="collapsible">
      <h1>TRAVEL -VISA</h1>
      <h3 class="font-roboto">Entry Formalities</h3>
      <p class="home-preview">Nationals from countries listed below who are travelling to the Philippines for business and tourism purposes are allowed to enter the Philippines without visas for a stay not exceeding twenty-one (21) days ...</p>
      <p class="read-more"><a href="../practical-information/travel-visa">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>PICC VENUE</h1>
      <p class="home-preview">The Philippine International Convention Center (PICC) has been the site of international, regional and local conventions in its 34 year history.</p>
      <p class="read-more"><a href="../practical-information/picc-venue">READ MORE</a></p>
    </div>
    <a href="../practical-information/tours-and-excursion" data-role="button" class="btn swatch-5 home-menu">TOURS AND EXCURSION</a>
    <a href="../practical-information/faq" data-role="button" class="btn swatch-5 home-menu">FAQ</a>
    <br/>
@stop