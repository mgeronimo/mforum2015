@extends('layouts.basic')

@section('content')
      <h1 class="font-roboto center page-title">FAQ</h1>

      <!--Registration FAQs-->
      <h3 class="font-roboto">1. Registration</h3>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: Where will the Forum 2015 be held?</h1>
        <p class="home-preview">A: Philippine International Convention Center (PICC), Vicente Sotto St., Pasay, Metro Manila, Philippines. The venue covers 12 hectares of reclaimed land fronting scenic Manila Bay. The PICC is less than 20 kilometers from the Ninoy Aquino International Airport (NAIA). Likewise, the PICC is within easy access to deluxe hotels, the city’s business centers and popular entertainment locales.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How can I register to attend FORUM 2015?</h1>
        <p class="home-preview">A: Click on the Register Now button located on top of the Forum 2015 site or click <a href="https://b-com.mci-group.com/Registration/FORUM2015.aspx" target="_blank">here</a>.</strong>.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How much does a registration fee cost and what does it include?</h1>
        <p class="home-preview">A: Conference registration fees vary based on your classification and also based on the time of registration. To determine your registration fee, please check our <a href="http://forum2015.org/index.php/get-involved/register" target="_blank">price grid</a>. The registration fee for regular international, local, and student delegates, includes entry to all conference sessions, exhibition and marketplace area, opening and closing session, access to delegate materials, and opportunity to submit abstract and present it.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: Do you offer a reduced fee for accompanying persons?</h1>
        <p class="home-preview">A: Yes, accompanying persons pay a reduced fee – see <a href="../get-involved/register">price grid</a>. This fee entitles them to join the networking events. Accompanying persons will not be entitled to attend any of the conference sessions or to receive delegate materials.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: What are the accepted methods of payment; can I pay the registration fee in EUR rather than USD?</h1>
        <p class="home-preview">A: Payments should be made in advance and in USD only, using a credit card or by bank transfer. For more information, check our <a href="../get-involved/register">price grid</a>.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How do I know that my registration is confirmed?</h1>
        <p class="home-preview">A: Your registration is confirmed upon receipt of your payment.</p>
      </div>

      <!--Participation, Visas and Accommodation FAQs-->
      <h3 class="font-roboto">2. Participation, Visas and Accommodation</h3>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: Do I need a visa to enter Philippines?</h1>
        <p class="home-preview">A: You may need a visa to enter Philippines, depending on your country of origin. If needed, delegates are strongly encouraged to apply for their visa as soon as possible, to ensure that it is issued on time. Confirmed participants will be issued an invitation letter, to be used in the visa application process, upon receipt of their registration payment.
            <ul>
              <li>Visa invitation letters will only be issued for the period of the event and for entry into the Philippines. No other date of requests or countries will be supported.</li>
              <li>Visa invitation letters will only be sent to the applicant once their registration is completed and payment has been made.</li>
              <li>Attendees are recommended to check the full requirements for a visa application, allowing plenty of time to provide the necessary documentation for the institution where the application is being processed through.</li>
              <li>Information provided and visa issuance is beyond the Forum 2015 organizers’control. Forum 2015 are not liable for visa processing and issuance /outcome.</li>
              <li>Please refer to our Terms & Conditions for cancellation conditions. There is a no refund policy in place. PCHRD will not refund registration fee in case of applicant's failure to obtain a visa.</li>
            </ul>
        </p>
        <p class="home-preview">For more information, please see the <a href="../practical-information/travel-visa">travel and visa information page</a>.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How can I book accommodation in Manila?</h1>
        <p class="home-preview">A: Visit the <a href="../practical-information/accommodation">accommodation page</a> to book your accommodation.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: Do I need travel insurance?</h1>
        <p class="home-preview">A: Forum 2015 organizers will not assume responsibility for any medical expenses that you or your family may incur whilst attending Forum, nor can grant compensation in the event of an accident, disability or death. It is, therefore, recommended that you insure against these risks as well as for travel.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q. What is the currency used in the Philippines? Where can I exchange my money?</h1>
        <p class="home-preview">A: The currency in the Philippines is Peso (PhP) and Centavo. 100 centavos = P1. Coin denominations are: 1, 5, 10, and 25 centavos, P1, and P5. Bill denominations are: 10, 20, 50, 100, 500 and 1, 000 pesos.</p>
        <p class="home-preview">Foreign currency may be exchanged at your hotel, and in most of the large department stores, banks and authorized money changing shops. Exchanging money anywhere else is illegal and the laws are strictly enforced.</p>
        <p class="home-preview">Most large stores, restaurants, hotels and resorts accept major credit cards including American Express, Visas and MasterCard. Traveler’s checks preferably American Express are accepted at hotels and large department stores.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How is the weather like in August in Manila?</h1>
        <p class="home-preview">A: The month of August is characterized by <em>essentially constant</em> daily high temperatures. The average probability that some form of precipitation will be observed in a given day is 76%, with little variation over the course of the month.</p>
      </div>

      <!--Interpretation and Translation FAQs-->
      <h3 class="font-roboto">3. Interpretation and Translation</h3>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: What languages will be used at the forum?</h1>
        <p class="home-preview">A: The official language of the forum is English.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: In what languages are the documents available?</h1>
        <p class="home-preview">A: The important documents are uploaded on our website and are available in English.</p>
      </div>

      <!--Programme and Abstract Submission FAQs-->
      <h3 class="font-roboto">4. Programme and Abstract Submission</h3>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: May I submit an abstract to the conference?</h1>
        <p class="home-preview">A: Visit the <a href="../get-involved/submit-an-abstract">abstract page</a> for more details.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How can I get involved in the programme of the conference?</h1>
        <p class="home-preview">A: You can contribute to our program by posting on our Interactive Discussions Page your suggestions and examples or contributing to our live debate graph.</p>
        <p class="home-preview">Visit our Program page for information on topics we are developing and get involved.</p>
      </div>

      <!--Satellite Sessions FAQs-->
      <h3 class="font-roboto">5. Satellite Sessions</h3>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: What are Satellite sessions?</h1>
        <p class="home-preview">A: Satellite sessions take place in or outside the conference center – outside the hours of the Forum agenda – but are fully organized and coordinated by the organization (private company, government agency, institution or NGO) hosting the satellite with support from the Forum secretariat. Satellite sessions slots are available for a fee, that is based on the room capacity and the time slot. Allocation of slots will depend on the availability on the Forum program. The content and speakers of the satellite meetings will be reviewed to ensure that they meet the principles of the Forum.</p>
      </div>
      <div data-role="collapsible">
        <h1 class="faq-qs">Q: How can I apply to organize a Satellite session?</h1>
        <p class="home-preview">A: Visit the Satellite Sessions page. Complete and send the satellite session application form. Your application will be reviewed by the Forum Secretariat and status confirmation will then be sent to you. You may write to <a href="mailto:secretariat@forum2015.org" target="_blank">secretariat@forum2015.org</a> to get a pricing on your potential Satellite Session.</p>
      </div>
      <br/>
      <p class="home-preview">Please don't hesitate to contact us at <a href="mailto:secretariat@forum2015.org">secretariat@forum2015.org</a> if you have any questions.</p>
@stop