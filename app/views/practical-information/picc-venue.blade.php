@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">PICC VENUE</h1>
	<div class="page-content">
		<p class="home-preview">The Philippine International Convention Center (PICC) has been the site of international, regional and local conventions in its 34 year history. These conferences cover a wide spectrum of disciplines from medicine, law, engineering, trade, social welfare and religious gatherings.</p>
		<p class="home-preview">Covering 12 hectares of reclaimed land fronting scenic Manila Bay, the PICC is less than 20 kilometers from the Ninoy Aquino International Airport (NAIA). Likewise, the PICC is within easy access to deluxe hotels, the city’s business centers and popular entertainment locales.</p>
		<p class="home-preview">Built on reclaimed land along scenic Manila Bay in the Philippine capital, the PICC has more than 60,000 square meters of floor area and a range of facilities that can accommodate almost any gathering of any size.</p>
		<p class="home-preview">The PICC has five (5) building modules, the Delegation Building, Secretariat Building, Plenary Hall, Reception Hall and The PICC Forum. Spaces herein are versatile enough to be transformed into almost any number of settings for any kind of exhibition, convention or event.</p>
		<br/><div id="picc">
			<img src="../images/picc/phoca_thumb_l_picc1.jpg">
			<img src="../images/picc/phoca_thumb_l_picc2.jpg">
			<img src="../images/picc/phoca_thumb_l_picc4.jpg" style="width: 100%; max-width: 437px;">
			<img src="../images/picc/phoca_thumb_l_picc5.jpg" style="width: 100%; max-width: 437px;">
			<img src="../images/picc/phoca_thumb_l_picc6.jpg" style="width: 100%; max-width: 437px;">
		</div><br/>
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7723.545919551891!2d120.9826!3d14.554972!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdecebc46f2b58867!2sPhilippine+International+Convention+Center!5e0!3m2!1sen!2sph!4v1419226878586" height="450" frameborder="0" style="width: 100%; border:0"></iframe>
	</div>
@stop