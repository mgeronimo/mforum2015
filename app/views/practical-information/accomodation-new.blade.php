@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">ACCOMODATION</h1>
	<div class="page-content">
		<p class="home-preview">The organizers secured hotel rooms conveniently located near the Philippine International Convention Center (PICC) at exclusive discounted rates. These special rates are only available through the registration process. We recommend you to book early to avail the limited discounted rates. Forum 2015's partner agency, INTAS, based in Manila, manages all hotel reservations and tours & excursion.</p>
		<p class="home-preview">Please send an email to <a href="mailto:accommodation@forum2015.org" target="_blank">accommodation@forum2015.org</a> for hotel reservation inquiries.</a>
		
		<!--List of Hotels-->
		<ul class="home-preview hotels">
			<li>
				<span class="hotel-name"><strong>Sofitel Philippine Plaza</strong></span><br/>
				<img src="../images/accomodation/sofitel.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>The only 5-star luxury resort hotel in Manila, Sofitel Philippine Plaza is an iconic property standing amid an expansive tropical setting. Known for its spectacular Manila Bay sunsets, the hotel continues to attract both local and international travelers alike with its signature brand of Filipino hospitality and the French joie de vivre that is reflected in its rituals in food and wine, and varied entertainment offerings.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Luxury Room (150)<br/>
				<span class="distance">SGL / DBL in USD:</span>198.00<br/>
				<span class="distance">Distance to Venue:</span>750 m<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Heritage Hotel</strong></span><br/>
				<img src="../images/accomodation/heritage.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>A 4-star deluxe hotel in Manila, the Heritage Hotel Manila is situated at the crossroads of Roxas Boulevard and EDSA, Metro Manila's central and main thoroughfares. <br/>This exceptional hotel is minutes away from both international and domestic airports, Manila Bay yacht clubs, Makati and Manila business hubs; PICC & World Trade Convention Center, SM Mall of Asia Complex featuring the country's largest shopping mall and SMX Convention Centre, the Philippines' biggest convention center and a new attraction in the field of entertainment, the Mall of Asia Arena.<br/>
				<span class="distance">Category:</span>4-star<br/>
				<span class="distance">Room Type:</span>Superior (200)<br/>
				<span class="distance">SGL / DBL in USD:</span>102.00<br/>
				<span class="distance">Distance to Venue:</span>5 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Manila Hotel</strong></span><br/>
				<img src="../images/accomodation/manila-hotel.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Defined by its history, elegance, and world-class service, the Manila Hotel maintains to be the choice hotel of the most distinguished clientele. Over the years, Manila Hotel continuously provides the best services and amenities for a truly memorable experience. <br/>The famous Café Ilang-Ilang was also renovated and was launched as a three-period meal buffet restaurant.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Grand Deluxe (50), Superior Deluxe (150)<br/>
				<span class="distance">SGL / DBL in USD:</span>116.00, 126.00<br/>
				<span class="distance">Distance to Venue:</span>4.4 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Pan Pacific Hotel</strong></span><br/>
				<img src="../images/accomodation/pan-pacific.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Nestled in the heart of a thriving metropolis is an elegant urban retreat that is ranked among the region’s finest by Condé Nast Traveler. Voted as the city’s best luxury accommodation by TripAdvisor, Pan Pacific Manila delivers an immersive experience that will refresh your senses.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Superior Room (40), Executive Room (40)<br/>
				<span class="distance">SGL / DBL in USD:</span>133.00, 144.00<br/>
				<span class="distance">Distance to Venue:</span>3.4 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>New World Manila Bay Hotel </strong>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> (Hyatt Regency Hotel and Casino)</span><br/>
				<img src="../images/accomodation/new-world-manila-bay.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Enjoy the convenience of easy access to the international airport, the business district, the largest shopping center in the city, and popular entertainment destinations, including well-preserved historical attractions such as Intramuros, Luneta Park, Rizal Park, Chinatown, and the Cultural Center of the Philippines. Business and leisure guests alike will appreciate the hotel's calm atmosphere, efficient service, and personal attention that is paid to their comfort and well-being.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Standard (70), City Bay Rooms (30), Clubrooms (30)<br/>
				<span class="distance">SGL / DBL in USD:</span>149.00, 163.00, 219.00<br/>
				<span class="distance">Distance to Venue:</span>3.2 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Diamond Hotel</strong></span><br/>
				<img src="../images/accomodation/diamond.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Set against the magnificent golden sunset of the Manila Bay is the five-star hotel Diamond Hotel Philippines. A luxury hotel in Manila with a sunset view, Diamond Hotel is only minutes away from the international and domestic airports, and from the Makati business district.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Superior (100)<br/>
				<span class="distance">SGL / DBL in USD:</span>126.00<br/>
				<span class="distance">Distance to Venue:</span>3.1 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Manila Peninsula</strong></span><br/>
				<img src="../images/accomodation/manila-peninsula.png" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>For over 38 years, The Peninsula Manila has set the benchmark for luxury and sophistication. Known affectionately as the “Jewel in the Capital’s Crown” for its legendary status and grand presence in the heart of Makati City. A luxurious haven of comfort, quality service and fine cuisine, the hotel is as much a favourite with discerning locals as it is with visitors.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Run of the House (150)<br/>
				<span class="distance">SGL / DBL in USD:</span>256.00<br/>
				<span class="distance">Distance to Venue:</span>8.4 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Dusit Thani Hotel</strong></span><br/>
				<img src="../images/accomodation/dusit-thani.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Dusit Thani Manila offers timeless Thai style and hospitality in the heart of Metro Manila. Situated in Makati, the financial capital of the Philippines, Dusit Thani Manila boasts easy access to a wealth of business, entertainment and recreational options.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Deluxe Room (150), Junior Suite (50)<br/>
				<span class="distance">SGL / DBL in USD:</span>140.00, 179.00<br/>
				<span class="distance">Distance to Venue:</span>7.1 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Makati Shangri-la Hotel</strong></span><br/>
				<img src="../images/accomodation/makati-shangri-la.png" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>A stay at Makati Shangri-La, Manila, is an unforgettable one. The hotel offers you the opportunity to experience the heart of Manila from the very center of Makati's most prestigious business, shopping and entertainment district.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Run of the House (200), Single Occupancy, Double Occupancy<br/>
				<span class="distance">SGL / DBL in USD:</span>286.00 (single occupancy), 314.00 (double occupancy)<br/>
				<span class="distance">Distance to Venue:</span>8.2 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Manila Pavilion Hotel</strong></span><br/>
				<img src="../images/accomodation/manila-pavilion.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Feel the warm and exceptional service that the Manila Pavilion Hotel has been known for throughout the years. Located in the heart of Manila’s business, cultural, shopping, and entertainment centers, experience the rich heritage and enjoy its finest accommodations with a stunning view of the park and bay.<br/>
				<span class="distance">Category:</span>4-star<br/>
				<span class="distance">Room Type:</span>Superior (100), Deluxe (100)<br/>
				<span class="distance">SGL / DBL in USD:</span>102.00, 109.00<br/>
				<span class="distance">Distance to Venue:</span>4.3 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Century Park Hotel</strong></span><br/>
				<img src="../images/accomodation/century-park.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Situated in the heart of Manila’s business, cultural, shopping and entertainment district is one of the most renowned and finest hotels in the metropolis. A mere seven (7) kilometers away from the Ninoy International Airport, Century Park Hotel - the premier hotel in Manila, is also an ideal airport hotel for busy executives.<br/>
				<span class="distance">Category:</span>4-star<br/>
				<span class="distance">Room Type:</span>Superior (150), Deluxe (50)<br/>
				<span class="distance">SGL / DBL in USD:</span>100.00, 109.00<br/>
				<span class="distance">Distance to Venue:</span>1.7 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Hotel H2O</strong></span><br/>
				<img src="../images/accomodation/hotel-h20.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Water is a predominant element in the Philippines. From tranquil, deep-blue seas teeming with marine life to scenic coastal areas ringed by splendid beaches... from bountiful rivers and lakes to tropical rainforests... this archipelago of more than 7,100 islands is blessed with an abundance of water in its various forms.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Run of the House (100)<br/>
				<span class="distance">SGL / DBL in USD:</span>160.00<br/>
				<span class="distance">Distance to Venue:</span>4.2 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>City Garden Suites</strong></span><br/>
				<img src="../images/accomodation/hotel-h20.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Right in the heart of Manila stands a benchmark of comfort and business-class luxury. City Garden Suites – Manila in Ermita, Manila, Philippines provides the perfect accommodation for business guests and leisure travelers seeking a home away from home at an affordable price.<br/>
				<span class="distance">Category:</span>3-star<br/>
				<span class="distance">Room Type:</span>Superior Room (30), Deluxe room (30)<br/>
				<span class="distance">SGL / DBL in USD:</span>74.00, 84.00<br/>
				<span class="distance">Distance to Venue:</span>4.0 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Armada Hotel</strong></span><br/>
				<img src="../images/accomodation/armada.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Armada Hotel Manila is in the heart of Manila meaning you have easy access to many of its attractions, like Cultural Centre of the Philippines (CCP). Harrison Plaza or Robinson's Place Manila give ample opportunity to do some shopping without having to go far from the hotel.<br/>
				<span class="distance">Category:</span>3-star<br/>
				<span class="distance">Room Type:</span>Superior (30), Deluxe (34)<br/>
				<span class="distance">SGL / DBL in USD:</span>81.00, 86.00<br/>
				<span class="distance">Distance to Venue:</span>2.2 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Solaire Resort and Casino</strong></span><br/>
				<img src="../images/accomodation/solaire.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Solaire is the first to open in the highly anticipated Entertainment City project along the Philippine’s famed Manila Bay.<br/>The first phase includes approximately 500 luxuriously appointed rooms, suites and bay side villas as well as resort-style pool facilities, bars and entertainment lounges. In addition, there is a state-of-the-art ballroom and meeting space, and a fully equipped spa and fitness center. The resort also features a world-class dining experience at its signature, casual and quick serve restaurants.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Deluxe City View (50), Deluxe Bayview (50), Sky Studio (100)<br/>
				<span class="distance">SGL / DBL in USD:</span>174.00, 198.00, 244.00<br/>
				<span class="distance">Distance to Venue:</span>4.2 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Fairmont and Raffles Residences</strong></span><br/>
				<img src="../images/accomodation/fairmont.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>The newest five-star luxury hotel in Makati City, Fairmont Makati is known for its prestigious address in the central business district of Manila, within walking distance to the Philippine Stock Exchange, Ayala Museum and Greenbelt - the city’s most stylish shopping area.<br/>
				<span class="distance">Category:</span>5-star<br/>
				<span class="distance">Room Type:</span>Fairmont Room (30), Deluxe Room (20)<br/>
				<span class="distance">SGL / DBL in USD:</span>174.00, 198.00<br/>
				<span class="distance">Distance to Venue:</span>7.9 km<br/>
			</li>
			<li>
				<span class="hotel-name"><strong>Midas Hotel</strong></span><br/>
				<img src="../images/accomodation/midas.jpg" class="accomodation-pic"/>
				<span class="distance">Short Description:</span>Midas Hotel is located where 3 key hubs of Metro Manila converge: Pasay City, home to the country's main international airport, SM Mall of Asia, and major embassies; the city of Manila, where cultural districts are found; and Makati City, the financial capital.<br/>
				<span class="distance">Category:</span>4-star<br/>
				<span class="distance">Room Type:</span>Deluxe (100), Executive (100)<br/>
				<span class="distance">SGL / DBL in USD:</span>121.00, 167.00<br/>
				<span class="distance">Distance to Venue:</span>3.5 km<br/>
			</li>
		</ul>
	</div>
@stop