@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">ACCOMODATION</h1>
	<div class="page-content">
		<p class="home-preview">The organizers secured hotel rooms conveniently located near the Philippine International Convention Center (PICC) at exclusive discounted rates. These special rates are only available through the registration process. We recommend you to book early to avail the limited discounted rates. Forum 2015's partner agency, INTAS, based in Manila, manages all hotel reservations and tours & excursion.</p>
		<p class="home-preview">Please send an email to <a href="mailto:accommodation@forum2015.org" target="_blank">accommodation@forum2015.org</a> for hotel reservation inquiries.</a>
		<h3 class="font-roboto">Headquarter Hotel</h3>
		<ul class="home-preview hotels">
			<li>
				<span class="hotel-name"><strong>Sofitel Philippine Plaza</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> CCP Complex, Pasay City</span><br/>
				<span class="distance">Distance from PICC:</span>0.3 kms / 5 mins.<br/>
				<span class="distance">Distance from Airport:</span>6.45 kms / 20 mins
			</li>
		</ul>
		<h3 class="font-roboto">Travel Grants Hotel</h3>
		<ul class="home-preview hotels">
			<li>
				<span class="hotel-name"><strong>Orchid Garden Suites</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Vito Cruz, Manila</span><br/>
				<span class="distance">Distance from PICC:</span>0.5 kms / 5 mins.<br/>
				<span class="distance">Distance from Airport:</span>7 kms / 20-30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Manila Pavilion</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> UN Ave., Manila/span></span><br/>
				<span class="distance">Distance from PICC:</span>2.0 kms / 15 mins.<br/>
				<span class="distance">Distance from Airport:</span>9 kms / 30 mins.
			</li>
		</ul>
		<h3 class="font-roboto">Other Hotels</h3>
		<ul class="home-preview hotels">
			<li>
				<span class="hotel-name"><strong>Century Park Hotel</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Vito Cruz, Manila</span><br/>
				<span class="distance">Distance from PICC:</span>0.5 km / 5 mins.<br/>
				<span class="distance">Distance from Airport:</span>7 kms / 20-30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Diamond Hotel Philippines</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Roxas Boulevard</span><br/>
				<span class="distance">Distance from PICC:</span>2 kms / 10 mins.<br/>
				<span class="distance">Distance from Airport:</span>79 kms / 20-30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Hyatt Hotel & Casino</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Pedro Gil, Manila</span><br/>
				<span class="distance">Distance from PICC:</span>1 km / 10 mins.<br/>
				<span class="distance">Distance from Airport:</span>8.21 kms / 20-30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Traders Hotel</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Roxas Boulevard</span><br/>
				<span class="distance">Distance from PICC:</span>1 km / 5 mins.<br/>
				<span class="distance">Distance from Airport:</span>6.49 kms / 20-30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Hotel H2O</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Rizal Park, Manila</span><br/>
				<span class="distance">Distance from PICC:</span>2.5 kms / 15 mins.<br/>
				<span class="distance">Distance from Airport:</span>9.6 kms / 30 mins.
			</li>
			<li>
				<span class="hotel-name"><strong>Microtel Inns & Suites</strong> &nbsp;&nbsp;|&nbsp;&nbsp;</span>
				<span class="hotel-loc"> Pasay City</span><br/>
				<span class="distance">Distance from PICC:</span>1 km / 10 mins.<br/>
				<span class="distance">Distance from Airport:</span>5.05 kms / 15-20 mins.
			</li>
		</ul>
		<p id="reference" style="text-align: center; font-size: 13px;">Hotel booking service will open in January 2015.</p>
	</div>
@stop