@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">Travel - Visa</h1>
	<div class="page-content">
		<p class="home-preview">Nationals from countries listed below who are travelling to the Philippines for business and tourism purposes are allowed to enter the Philippines without visas for a stay not exceeding twenty-one (21) days, provided they hold valid tickets for their return journey to port of origin or next port of destination and their passports valid for a period of at least six (6) months beyond the contemplated period of stay. However, Immigration Officers at ports of entry may exercise their discretion to admit holders of passports valid for at least sixty (60) days beyond the intended period of stay.</p>
		<p class="home-preview">Nationals from the following countries are allowed to enter the Philippines without a visa for a period of stay of twenty-one (21) days or less:</p>
		<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
			<tbody>
				<tr style="height: 213.3pt;">
				<td style="width: 33.33%; padding: 0in 5.4pt; height: 213.3pt;" valign="top" width="150">
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Andorra</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Angola </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Antigua and Barbuda Argentina </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Australia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Austria </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Bahamas </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Bahrain </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Barbados </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Belgium</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Benin </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Bhutan </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Bolivia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Botswana </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Brazil* </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Brunei Darussalam</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Bulgaria </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Burkina Faso </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Burundi </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Cambodia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Cameroon </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Canada </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Cape Verde </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Central African Republic Chad </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Chile </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Colombia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Comoros </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Congo </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Costa Rica</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Cote d’Ivoire </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Cyprus </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Czech Republic </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Democratic Republic of the Congo </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Denmark </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Djibouti </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Dominica </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Dominican Republic </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Ecuador </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">El Salvador </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Equatorial </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Guinea </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Eritrea </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Estonia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Ethiopia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Fiji </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Finland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">France </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Gabon </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Gambia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Germany </span></p>
				</td>
				<td style="width: 33.33%; padding: 0in 5.4pt; height: 213.3pt;" valign="top" width="150">
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Ghana </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Gibraltar </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Greece </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Grenada </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Guatemala </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Guinea </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Guinea Bissau </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Guyana </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Haiti </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Honduras </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Hungary </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Iceland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Indonesia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Ireland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Israel* </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Italy </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Jamaica </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Japan </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Kenya </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Kiribati </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Kuwait </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Lao People’s Democratic Republic </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Latvia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Lesotho </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Liberia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Liechtenstein </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Lithuania </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Luxembourg </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Madagascar </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Malawi </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Malaysia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Maldives </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mali </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Malta </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Marshall Islands </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mauritania </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mauritius </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mexico </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Micronesia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Monaco </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mongolia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Morocco </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Mozambique </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Myanmar </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Namibia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Nepal </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Netherlands </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">New Zealand </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Nicaragua </span></p>
				</td>
				<td style="width: 33.33%; padding: 0in 5.4pt; height: 213.3pt;" valign="top" width="150">
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Niger </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Norway </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Oman </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Palau </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Panama </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Papua New Guinea Paraguay </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Peru </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Poland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Portugal </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Qatar </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Republic of Korea </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Romania </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Russia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Rwanda </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Saint Kitts and Nevis </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Saint Lucia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Saint Vincent and the Grenadines </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Samoa </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">San Marino </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Sao Tome and Principe Saudi Arabia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Senegal </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Seychelles </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Singapore </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Slovakia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Slovenia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Solomon Islands </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Somalia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">South Africa </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Spain </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Suriname </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Swaziland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Sweden </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Switzerland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Thailand </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Togo </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Trinidad and Tobago Tunisia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Turkey </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Tuvalu</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Uganda</span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">United Arab Emirates United Kingdom of Great Britain and Northern Ireland </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">United Republic of Tanzania </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">United States of America Uruguay </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Venezuela </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Vietnam </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Zambia </span></p>
					<p style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 9pt; color: black;">Zimbabwe </span></p>
				</td>
				</tr>
			</tbody>
		</table>
		<p class="home-preview">The following are allowed to enter the Philippines without a visa for a stay not exceeding fifty-nine (59) days:</p>
		<ol class="home-preview">
			<li>Holders of Brazil passports; and</li>
			<li>Holders of Israel passports</li>
		</ol>
		<p class="home-preview">The following are allowed to enter the Philippines without a visa for a stay not exceeding fourteen (14) days:</p>
		<ol class="home-preview">
			<li>Holders of Hong Kong Special Administrative (SAR) passports.</li>
			<li>Holders of Macau Special Administrative Region (SAR) passports.</li>
		</ol>
		<p class="home-preview">The following are allowed to enter the Philippines without a visa for a stay not exceeding seven (7) days:</p>
		<ol class="home-preview">
			<li>Holders of Macau-Portuguese passports</li>
			<li>Holders of Hong Kong British passports.</li>
		</ol>
		<br/>
		<h3 class="font-roboto">Accessibility by Air</h3>
		<p class="home-preview">Manila, Cebu, Davao, Clark, Subic, and Laoag are the international gateways, with the Ninoy Aquino International Airport (NAIA) in Manila as the premier gateway. It is served by more than 30 airlines, which fly to different cities around the world. The Mactan International Airport (MIA) in Cebu handles regular flights from Japan, Singapore, and Australia as well as chartered flights from Hong Kong, the United States, and other major travel capitals. Davao International Airport handles regular flights from Indonesia and Singapore. The Diosdado Macapagal International Airport and Subic Airfield in Central Luzon service both chartered and cargo planes. Laoag International Airport in Ilocos Norte services regular flights from Taiwan and Macau.</p>
		<h3 class="font-roboto">Accessibility by Sea</h3>
		<p class="home-preview">As the islands of the Philippines are separated by different bodies of water, the sea plays an integral part in travel. A range of seafarers are available, from huge cargo ships to small ferry boats; take long trips that last for a day or two with regular ship lines or take shorter ones with ferries. Major cruise liners call on the port of Manila.</p>
		<h3 class="font-roboto">Custom Regulations</h3>
		<p class="home-preview">To facilitate customs examination, visitors are advised to fill in the Baggage and Currency Declaration Form before disembarking. Visitors are allowed to bring the following items duty-free:</p>
		<ol class="home-preview">
			<li>Personal effects – A reasonable amount of clothing for personal use and a small quantity of perfume.</li>
			<li>Tobacco and alcoholic beverages – 400 sticks of cigarettes, or 2 tins of smoking tobacco, and 2 bottles of alcoholic beverages of not more than 1 liter each.</li>
		</ol>
		<h3 class="font-roboto">Airport Facilities</h3>
		<p class="home-preview">The Philippines has four (4) international airports: the Ninoy Aquino International Airport (NAIA) in Manila, Diosdado Macapagal International Airport (DMIA) in Angeles City, Mactan-Cebu International Airport in Lapu-Lapu City, Cebu and the Francisco Bangoy International Airport in Davao City.</p>
		<p class="home-preview">The NAIA is the main international gateway of the country. It is seven (7) kilometers south of Manila and southwest of Makati City’s Central Business District. It has three (3) terminals that serve Manila and its surrounding metropolitan area. Manila is also served by the Manila Domestic Airport which is one kilometer from the NAIA.</p>
		<p class="home-preview">These international airports have adequate traveler facilities: duty-free shopping centers, souvenir shops, tourist information counters, hotel and travel agency representatives and car rental services. The NAIA has banks, postal service, a medical clinic, a pharmacy, chapels, salon, sauna, and massage services.</p>
		<h3 class="font-roboto">Transportation within the City</h3>
		<p class="home-preview">A range of public transport facilities are available in Metropolitan Manila. The traditional mode of transport for most Filipinos is the jeepney aptly called “folk art wheels.” They ply most of Manila’s secondary routes and even a few major thoroughfares.</p>
		<p class="home-preview">The elevated Light Rail Transit (LRT) 1 & 2 and the Metro Rail Transit (MRT) are the fastest ways to get from one end of the Metropolis to the other at a reasonable cost. Air conditioned buses ply all major routes in the metropolis.</p>
		<p class="home-preview">Air conditioned taxis are readily available at all hotels and throughout the city. They can be flagged down and hailed from the street with a flag-down rate of PhP40 and PhP 3.50 per 300m for succeeding kilometers.</p>
		<p class="home-preview">Most of the major international car rental agencies, like Hertz and Avis, have offices in Metro Manila and carry a full line of economy and luxury vehicles for rent. For those wishing to rent self-drive vehicles, a valid foreign or international license is required.</p>
		<h3 class="font-roboto">Official invitation letters</h3>
		<p class="home-preview">To support visa applications for Forum participants, a letter of invitation will be available to all fully registered delegates. Your letter will be retrieved automatically as you register online for the Forum, once your registration fee is settled.</p>
		<p class="home-preview">Please contact <a href="mailto:secretariat@forum2015.org">secretariat@forum2015.org</a> to retrieve your letter.</p>
		<h3 class="font-roboto">Discounted flights to Manila</h3>
		<p class="home-preview">We are currently seeking for airways partnership to offer flight discount to our delegates. Get the latest news by subscribing to the newsletter: <a href="http://forum2015.org/index.php/forum-2015-newsletter-subscribe">Subscribe</a>!</p>
	</div>
@stop