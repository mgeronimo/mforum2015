@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">FORUM 2015 Secretariat</h1>
  	<div class="page-content">
        <br/>
  		  <h3 class="font-roboto">Physical Address</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-map-marker"></i></p></td>
              <td>
                3rd Flr DOST Main Building<br/>
                Gen. Santos Ave., Bicutan Taguig City1631 Philippines
              </td>
            </tr>
        </table>
        <h3 class="font-roboto">Contact Numbers</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-phone"></i></p></td>
              <td>
                +632 8377534<br/>
                +632 8377537
              </td>
            </tr>
        </table>
        <h3 class="font-roboto">Web Address</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-globe"></i></p></td>
              <td>
                <a href="http://www.forum2015.org">www.forum2015.org</a>
              </td>
            </tr>
        </table>
        <h3 class="font-roboto">Registration Inquiries</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-home"></i></p></td>
              <td>
                <a href="mailto:registration@forum2015.org">regstration@forum2015.org</a>
              </td>
            </tr>
        </table>
        <h3 class="font-roboto">Accomodation Inquiries</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-home"></i></p></td>
              <td>
                <a href="mailto:accomodation@forum2015.org">accomodation@forum2015.org</a>
              </td>
            </tr>
        </table>
        <h3 class="font-roboto">General Inquiries</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-info-circle"></i></p></td>
              <td>
                <a href="mailto:secretariat@forum2015.org">secretariat@forum2015.org</a>
              </td>
            </tr>
        </table>  
        <h3 class="font-roboto">Travel Grants</h3>
        <table>
            <tr>
              <td><p class="contact-icons"><i class="icon-home"></i></p></td>
              <td>
                <a href="mailto:travelgrants@forum2015.org">travelgrants@forum2015.org</a>
              </td>
            </tr>
        </table>
	</div>
	<br/>
@stop