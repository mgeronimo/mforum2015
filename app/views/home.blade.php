@extends('layouts.basic')

@section('content')
      <img src="images/below-logo-text.png" id="below-logo" /><br/>
      <div data-role="collapsible">
        <h1>WHY ATTEND FORUM 2015?</h1>
        <ul class="home-preview">
          <li>To <strong>learn</strong> and <strong>identify</strong> new solutions to the world’s unmet health needs</li>
          <li>To <strong>engage</strong> with influential actors and leaders in research and innovation for health (e.g. senior government officials)</li>
        </ul>
        <p class="read-more"><a href="why-attend-forum-2015">READ MORE</a></p>
      </div>
      <div data-role="collapsible">
        <h1>IMPORTANT DATES</h1>
        <p class="home-preview impt-dates center"><strong>30 January 2015</strong><br/>Abstract submission opens</p>
        <p class="home-preview impt-dates center"><strong>30 March 2015</strong><br/>Call for session deadline</p>
        <p class="read-more"><a href="important-dates">READ MORE</a></p>
      </div>
      <div data-role="collapsible">
        <h1>PROGRAM THEMES</h1>
        <h3 class="font-roboto">Programme pillars and themes</h3>
        <ol class="home-preview">
          <li>Increasing the Effectiveness of Research and Innovation for Health (through):
            <ul>
              <li>Social Accountability</li>
              <li>Increasing investments</li>
              <li>Country-driven capacity building</li>
            </ul>
          </li>
        </ol>
        <p class="read-more"><a href="programme-at-a-glance">READ MORE</a></p>
      </div>
      <div data-role="collapsible">
        <h1>ABOUT FORUM 2015</h1>
        <p class="home-preview">The Global Forum on Research and Innovation for Health 2015 aims to identify solutions to the world’s unmet health needs through research and innovation.</p>
        <p class="read-more"><a href="about-forum-2015">READ MORE</a></p>
      </div>
      <!--<a href="sponsors" data-role="button" class="btn swatch-5 home-menu">SPONSORS</a>-->
      
@stop