@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">DOWNLOADS</h1>
  	<div class="page-content">
      <h3 class="font-roboto">Logos</h3>
      <ul class="home-preview">
        <li><a href="../download/dost_with_caption.png" target="_blank" download>DOST with Caption</a></li>
        <li><a href="../download/pchrd_with_caption.png" target="_blank" download>PCHRD with Caption</a></li>
        <li><a href="../download/doh.png" target="_blank" download>DOH</a></li>
        <li><a href="../download/cohred.png" target="_blank" download>COHRED</a></li>
        <li><a href="../download/forum_2015.png" target="_blank" download>Forum 2015</a></li>
      </ul>
      <h3 class="font-roboto">Banners</h3>
      <ul class="home-preview">
        <li><a href="../download/site-banner.png" target="_blank" download>Banner</a></li>
        <li><a href="../download/cover-photo.png" target="_blank" download>Cover Photo</a></li>
      </ul>
	</div>
	<br/>
@stop