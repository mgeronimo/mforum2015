@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">BACKGROUND AND OBJECTIVES</h1>
	<div class="page-content">
        <p>The Global Forum on Research and Innovation for Health 2015 aims to identify solutions to the world’s unmet health needs through research and innovation.</p>
        <p>As world leaders shift their focus to the post-2015 Sustainable Development Goals, increased understanding of the impact of research and innovation on national health systems is of critical importance. Scientific research and innovation has transformative effects on not only health outcomes, but also on national economic growth and sustainable development.</p>
        <p>Forum 2015 provides a platform where low and middle income countries take prime position in defining the global health research agenda that better suits their needs, in presenting solutions and in creating effective partnerships for action. Putting “People at the Center of Health Research and Innovation”, Forum 2015 will place much emphasis on empowering populations in low- and middle-income countries as leading role actors of their own future.</p>
        <p>The Council on Health Research for Development (COHRED), in partnership with the Philippine Department of Health and Philippine Department of Science and Technology, will host the Global Forum on Research and Innovation for Health in Manila, from 24-27 August 2015.</p>
        <p>Forum 2015 will bring together all stakeholders who play a role in making research and innovation benefit health, equity and development. This includes high-level representatives from government, business, non-profits, international organizations, academic and research institutions and social entrepreneurs among others.</p>
        <p>The Global Forum for Research and Innovation is the successor to the Global Forum for Health Research, last held in Cape Town, South Africa in 2012.</p>
        <br/>
	</div>
	<br/>
@stop