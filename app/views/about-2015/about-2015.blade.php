@extends('layouts.basic')

@section('content')
    <h1 class="font-roboto center page-title">ABOUT FORUM 2015</h1>
    <div data-role="collapsible">
      <h1>BACKGROUND AND OBJECTIVES</h1>
      <p class="home-preview">The Global Forum on Research and Innovation for Health 2015 aims to identify solutions to the world’s unmet health needs through research and innovation.</p>
      <p class="read-more"><a href="about-2015/background-and-objectives">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>COHRED</h1>
      <h3 class="font-roboto">Who We Are</h3>
      <p class="home-preview">The Council on Health Research for Development (COHRED) is an international non-profit and non-governmental organisation based in Geneva, Switzerland – Gaborone, Botswana – and Belo Horizonte, Brazil.</p>
      <p class="read-more"><a href="about-2015/cohred">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>DOST</h1>
      <p class="home-preview">The Department of Science and Technology (DOST) is the premiere science and technology body in the country charged with the twin mandate of providing central direction, leadership and coordination of all scientific and technological activities ...</p>
      <p class="read-more"><a href="about-2015/dost">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>DOH</h1>
      <p class="home-preview">The Philippine Department of Health (DOH) is the executive department of the Philippine government responsible for ensuring access to basic public health services by all Filipinos through the provision of quality health care ...</p>
      <p class="read-more"><a href="about-2015/doh">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>PCHRD</h1>
      <p class="home-preview">The Philippine Council for Health Research and Development (PCHRD) is one of the three sectoral councils of the Department of Science and Technology (DOST).</p>
      <p class="read-more"><a href="about-2015/pchrd">READ MORE</a></p>
    </div>
    <a href="../sponsors" data-role="button" class="btn swatch-5 home-menu">PARTNERS AND SPONSORS</a>
    <div data-role="collapsible">
      <h1>PHILIPPINE NATIONAL HEALTH RESEARCH SYSTEM</h1>
      <p class="home-preview">The Philippine National Health Research System (PNHRS) is an integrated national framework for health research that aims to promote cooperation and integration of all health research efforts and stakeholders in the country ...</p>
      <p class="read-more"><a href="about-2015/health-research-in-the-phil">READ MORE</a></p>
    </div>
    <a href="../contact-us" data-role="button" class="btn swatch-5 home-menu">CONTACT</a>
    <!--<a href="sponsors" data-role="button" class="btn swatch-5 home-menu">SPONSORS</a>-->

@stop