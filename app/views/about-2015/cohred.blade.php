@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">COHRED</h1>
	<div class="page-content">
		<h3 class="font-roboto">Who We Are</h3>
    	<p class="home-preview">The Council on Health Research for Development (COHRED) is an international non-profit and non-governmental organisation based in Geneva, Switzerland – Gaborone, Botswana – and Belo Horizonte, Brazil. We have over 20 years’ hands-on experience in providing expertise, tools and solutions to enable low and middle income countries to develop and use research and innovation to improve health of those with most unmet health needs.</p>
    	<h3 class="font-roboto">Our Vision</h3>
    	<p class="home-preview">Accelerate the development of research and innovation systems in low and middle income countries – in ways that promote health of those who need it most.</p>
    	<h3 class="font-roboto">Our Mission</h3>
    	<p class="home-preview">Maximize the potential of research and innovation to improve health, equity and sustainable development around the world. We prioritise work that impacts low income countries.</p>
    	<h3 class="font-roboto">What We Do</h3>
    	<p class="home-preview">
            <ol class="home-preview">
                <li>We provide expertise and technical support to governments, research institutions, and to non-profit and for profit organizations working in research and innovation for health</li>
                <li>We design tools to address bottlenecks in research and innovation system optimization for health and equity</li>
                <li>We engage in global actions to create unique opportunities for meetings of minds to generate increasingly complex solutions and partnerships needed to solve complex global health challenges through research and innovation.</li>
            </ol>
        </p>
		<h3 class="font-roboto">What We Can Do For You</h3>
    	<p class="home-preview">Whether you are a government or government institution; a research and academic centre; a for-profit business or non-profit group; a bilateral or multilateral organisation; a research sponsor or philanthropy; and even an individual – who is engaged in and passionate about funding, supporting, promoting, using, translating and evaluating research to improve health, equity and sustainable socio-economic development, we can add value to your business or activities through our own actions, our global network of key contacts, and our independent and ethical approach to finding solutions aimed at making you better at what you do.</p>
    	<p class="home-preview">For more information visit <a href="http://cohred.org" target="_blank">cohred.org</a> or contact us at <a href="mailto: cohred@cohred.org" target="_blank">cohred@cohred.org</a></p>
	</div>
@stop