@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">DOH</h1>
  	<div class="page-content">
        <p>The Philippine Department of Health (DOH) is the executive department of the Philippine government responsible for ensuring access to basic public health services by all Filipinos through the provision of quality health care and the regulation of all health services and products.</p>
        <p>The department has three major roles in the health sector: (1) leadership in health; (2) enabler and capacity builder; and (3) administrator of specific services. Its mandate is to develop national plans, technical standards, and guidelines on health. Aside from being the regulator of all health services and products, the DOH is the provider of special tertiary health care services and technical assistance to health providers and stakeholders.</p>
        <p>Please visit <a href="http://www.doh.gov.ph">doh.gov.ph</a>.</p>
	</div>
	<br/>
@stop