@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">PCHRD</h1>
  	<div class="page-content">
        <p>The Philippine Council for Health Research and Development (PCHRD) is one of the three sectoral councils of the Department of Science and Technology (DOST). It is a forward-looking, partnership-based national body responsible for coordinating and monitoring research activities in the country.</p>
        <p>As the national coordinating body for health research, PCHRD provides central direction, leadership and coordination of health S&T. The Council envisions <em>“A healthy Filipino nation that benefits from research-based solutions and innovations”</em>.</p>
        <p>Please visit <a href="http://pchrd.dost.gov.ph">pchrd.dost.gov.ph</a>.</p>
	</div>
	<br/>
@stop