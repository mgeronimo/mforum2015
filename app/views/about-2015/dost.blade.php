@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">DOST</h1>
  	<div class="page-content">
        <p>The Department of Science and Technology (DOST) is the premiere science and technology body in the country charged with the twin mandate of providing central direction, leadership and coordination of all scientific and technological activities, and of formulating policies, programs and projects to support national development.</p>
        <p>The department envisions "A reservoir of scientific and technological know-how providing world-class solutions that empower Filipinos to attain higher productivity and better quality of life”. The DOST adheres to the declared state policy of supporting local scientific and technological effort; develops local capability to achieve technological self-reliance; and encourage greater private sector participation in research and development.</p>
        <p>Please visit <a href="http://dost.gov.ph">dost.gov.ph</a>.</p>
	</div>
	<br/>
@stop