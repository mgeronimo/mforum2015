@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">The Philippine National Health Research System</h1>
  	<div class="page-content">
        <p>The Philippine National Health Research System (PNHRS) is an integrated national framework for health research that aims to promote cooperation and integration of all health research efforts and stakeholders in the country to ensure that research contributes to evidence-informed health policies and actions.</p>
        <p>By virtue of Republic Act No. 10532 or the Philippine National Health Research System (PNHRS) Act of 2013 defines PNHRS as the integrated national framework and convergence strategy for health research in the country.</p>
        <p>Anchored on the following core values of inclusiveness, innovativeness and creativity, responsiblity, ethics, accountability, synergy and equity, it envisions a vibrant, dynamic and responsible health research community working for the attainment of national and global health goals; by creating and sustaining an enabling environment that will generate knowledge, innovation, technology, products and services to promote health and well-being of every Filipino.</p>
        <p>For more information, visit <a href="http://www.healthresearch.ph">healthresearch.ph</a>.</p>
	</div>
	<br/>
@stop