@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">SPONSORS AND MARKETPLACE</h1>
  	<div class="page-content">
		<h3 class="font-roboto">Marketplace</h3>
		<p class="home-preview">The Marketplace is the heart of the Forum 2015. It provides a central and dynamic environment where delegates can network, exchange ideas, but also participate in a range of activities scheduled throughout the whole duration of the Forum. In the Marketplace, delegates will be able to:
			<ul class="home-preview">
				<li>Engage with several international organizations who will be showcasing their activities in the international exhibition</li>
				<li>Listen to and interact with poster presenters, whom will be presenting their work</li>
				<li>Interact and network in the lounge and coffee break area</li>
				<li>Organize a meeting to connect with other participants.</li>
			</ul>
		</p>
		<h3 class="font-roboto">Sponsorship</h3>
		<p class="home-preview">The Global Forum on Research and Innovation for Health will provide a high level opportunity:
			<ul class="home-preview">
				<li>To demonstrate your commitment to the use research and innovation in finding solutions for the world’s health needs</li>
				<li>To share your actions and programs through a respected and influential global platform</li>
				<li>To engage and exchange knowledge and information with the most influential actors in research and innovation for health including key leaders (heads of state, ministers of health, senior academics, leading researchers, industry, NGOs, CSO and health care professionals)</li>
				<li>To reach a global audience by participating in all the pre event, event and post event and interactive discussions</li>
				<li>To take part in one of the largest events in Southeast Asia  in conjunction with ASEAN economic community launch and the 2015 APEC summit</li>
				<li>To get to know the Philippines – one of the fastest growing economies in the world. A market of over 100 million people, a GDP of 270 billion dollars and over 5% solid growth for a decade</li>
				<li>To gain insight into research and innovation in Southeast Asia</li>
				<li>To actively engage with low and middle income countries as they set their own agendas for health research and innovation.</li>
			</ul>
		</p>
		<p class="home-preview">The Sponsorship and Market Place guidelines will be soon available, please get the latest news by subscribing to the newsletter: <a href="http://forum2015.org/index.php/forum-2015-newsletter-subscribe" target="_blank">Subscribe!</a></p>
	</div>
@stop  