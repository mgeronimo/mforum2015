@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">GET INVOLVED</h1>
    <div data-role="collapsible">
      <h1>REGISTER</h1>
      <p class="home-preview">Online registration for delegates will open on the 12th January. Please note that Invited Speakers, Sponsors and Exhibitors who are entitled for complimentary registrations will be provided with a different registration link.</p>
      <p class="read-more"><a href="../get-involved/register">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>SUBMIT AN ABSTRACT</h1>
      <h3 class="font-roboto">Abtract Topics</h3>
      <ol type="I" class="home-preview">
        <li>
          Increasing the Effectiveness of Research and Innovation for Health (through):
          <ul>
            <li>Social Accountability</li>
            <li>Increasing investments</li>
            <li>Country-driven capacity building</li>
          </ul>
        </li>
      </ol>
      <p class="read-more"><a href="../get-involved/submit-an-abstract">READ MORE</a></p>
    </div>
    <div data-role="collapsible">
      <h1>SPONSORSHIP AND MARKETPLACE</h1>
      <p class="home-preview">The Marketplace is the heart of the Forum 2015. It provides a central and dynamic environment where delegates can network, exchange ideas, but also participate in a range of activities ...</p>
      <p class="read-more"><a href="../get-involved/sponsorship-and-marketplace">READ MORE</a></p>
    </div>
    <br/>
@stop