@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">SUBMIT AN ABSTRACT</h1>
	<div class="page-content">
		<h3 class="font-roboto">Abtract Topics</h3>
		<ol type="I" class="home-preview">
			<li>
				Increasing the Effectiveness of Research and Innovation for Health (through):
				<ul>
					<li>Social Accountability</li>
					<li>Increasing investments</li>
					<li>Country-driven capacity building</li>
				</ul>
			</li>
			<li>
				The role of research and innovation (in improving):
				<ul>
					<li>Food safety and security</li>
					<li>Health in mega-cities</li>
					<li>Disaster risk reduction</li>
				</ul>
			</li>
		</ol>
		<p class="home-preview">Information about abstract submission for Forum 2015 will be posted here shortly.</p>
		<h3 class="font-roboto">Important dates</h3>
		<p class="home-preview">
			<table class="home-preview" cellpadding="3" style="text-align: left;">
				<tr>
					<td width="50%">30 January 2015</td>
					<td>Abstract submission opens</td>
				</tr>
				<tr>
					<td>30 March 2015</td>
					<td>Call for session deadline</td>
				</tr>
				<tr>
					<td>31 March 2015</td>
					<td>Abstract submission deadline</td>
				</tr>
				<tr>
					<td>June 2015</td>
					<td>Abstract notification letter</td>
				</tr>
				<tr>
					<td>June 2015</td>
					<td>Abstract notification letter</td>
				</tr>
			</table>
		</p>
	</div>
@stop