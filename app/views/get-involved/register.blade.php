@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">REGISTER</h1>
	<div class="page-content">
		<p class="home-preview">Online registration for delegates will open on the 12th January. Please note that Invited Speakers, Sponsors and Exhibitors who are entitled for complimentary registrations will be provided with a different registration link.</p>
		<p class="home-preview">As you register online for the Forum 2015, you will be able to:
			<ul class="home-preview">
				<li>Book your hotel accommodation in Manila at exclusive discounted rates - limited availability</li>
				<li>Book a tour / excursion</li>
				<li>Request an invitation letter - VISA application</li>
			</ul>
		</p><br/>
		<h3 class="font-roboto">Price grid (USD)</h3>
		<div class="home-preview">
			<h4 class="register-subs">International</h4>
			<strong>Early Registration</strong> (until 19, June 2015): 620<br/>
			<strong>Standard Registration</strong> (from 20, June 2015): 730<br/>
			<strong>Onsite Registration</strong> (as of 08 August 2015): 840
		</div>
		<div class="home-preview">
			<h4 class="register-subs">Philippines</h4>
			<strong>Early Registration</strong> (until 19, June 2015): 390<br/>
			<strong>Standard Registration</strong> (from 20, June 2015): 500<br/>
			<strong>Onsite Registration</strong> (as of 08 August 2015): 560
		</div>
		<div class="home-preview">
			<h4 class="register-subs">Student</h4>
			<strong>Early Registration</strong> (until 19, June 2015): 45<br/>
			<strong>Standard Registration</strong> (from 20, June 2015): 45<br/>
			<strong>Onsite Registration</strong> (as of 08 August 2015): 45
		</div>
		<div class="home-preview">
			<h4 class="register-subs">Accompanying Person</h4>
			<strong>Early Registration</strong> (until 19, June 2015): 225<br/>
			<strong>Standard Registration</strong> (from 20, June 2015): 225<br/>
			<strong>Onsite Registration</strong> (as of 08 August 2015): 225
		</div><br/>
		<h3 class="font-roboto">Terms and condition of registration</h3>
		<p class="home-preview">The following activities are included in the Full participant registration:
			<ul class="home-preview">
				<li>Access to all sessions</li>
				<li>Access to the Marketplace</li>
				<li>Invitation to the Welcome Reception and Opening Ceremony</li>
				<li>Delegate material (including printed Final Programme if requested in advance, access to Forum Smart Phone application which includes the e-networking Platform and sponsors' materials)</li>
				<li>Opportunity to submit and present an abstract</li>
				<li>Lunches on Tuesday, Wednesday and Thursday</li>
				<li>Coffee breaks on Tuesday, Wednesday and Thursday</li>
				<li>Free shuttle service from hotel to forum venue</li>
			</ul>
		</p>
		<p class="home-preview">The following activities are included in the Accompanying person registration:
			<ul class="home-preview">
				<li>Invitation to the Welcome Reception and Opening Ceremony</li>
				<li>Half day city tour</li>
			</ul>
		</p>
		<h3 class="font-roboto">Onsite registration opening hours</h3>
		<p class="home-preview">Available soon</p>
		<h3 class="font-roboto">Payments</h3>
		<p class="home-preview">Registration is valid on receipt of payment. Payments can be made by telegraphic transfer or credit card. Upon receipt of your completed registration form and payment, you will receive a confirmation of your registration by e-mail. Early bird fee is available until 19 June 2015. Visa invitation letter requests are processed immediately following the completion of your registration and are submitted to you via email. You must ensure you provide all the mandatory information before your request will be processed.</p>
		<p class="home-preview">Visa invitation letters will only be issued for the period of the event and for entry to the Philippines. No other date and / or country requests will be supported. </p>
		<p class="home-preview">Available payment options for International and Local delegates include:
			<ul class="home-preview">
				<li><strong>Credit Card</strong> - VISA or Master Card</li>
				<li><strong>Telegraphic transfer</strong> - you will be receiving banking instructions through email. Bank details for a Telegraphic transfer can be found on your invoice.</li>
			</ul>
		</p>
		<h3 class="font-roboto">Cancellation Policy</h3>
		<p class="home-preview">All cancellation requests must be submitted in official writing by email to MCI Group Asia Pacific Pte Ltd, The Official Registration and Accommodation Agency. For all cancellation requests received before and on 24 June 2015, a canellation fee of 50% of the total registration fees will be applicable. No refunds will be permitted for cancellations reeived starting 25 June 2015. All refunds will be made within 3 months after the event. Kindly email <a href="mailto:registration@forum2015.org" target="_blank">registration@forum2015.org</a>.</p>
		<h3 class="font-roboto">Lost Badges</h3>
		<p class="home-preview">In the event of a lost or forgotten badge, an administrative fee of US$10 will be charged for the reprint of the badge after verification of identification (passport, driving license or other recognized identification paper).</p>
		<h3 class="font-roboto">Name Changes</h3>
		<p class="home-preview">All modification and substitution requests must be submitted in official writing by email to MCI Group Asia Pacific Pte Ltd, The Official Registration and Accommodation Agency. For replacement of a delegate under individual registration, written permission from the original registered delegate will be required, along with full contact details of the substituted delegate.  An administrative fee of US$100 will be applicable for such requests received after 24 July 2015. Kindly email <a href="mailto:registration@forum2015.org" target="_blank">registration@forum2015.org</a>.</p>
		<h3 class="font-roboto">Insurance</h3>
		<p class="home-preview">The Registration fee does not include insurance of any kind and the Forum Secretariat cannot take any responsibility for any participant failing to arrange their own insurance. Delegates are encouraged to make their own insurance arrangements to cover any loss caused by unforeseen delay, circumstance or cancellation.</p>
		<h3 class="font-roboto">Official invitation letters</h3>
		<p class="home-preview">To support visa applications for Forum participants, a letter of invitation is available to all registered delegates.</p>
		<p class="home-preview">Visa invitation letter requests are processed immediately following the completion of your registration, once the payment has been received. You must ensure you provide all the mandatory information before your request will be processed.</p>
		<p class="home-preview">Depending on your country of origin, you may need a visa to enter Philippines go to our <a href="../practical-information/travel-visa">Travel and Visa</a> page  for more information. If needed, delegates are strongly encouraged to apply for their visa as soon as possible, to ensure that it is issued on time. Confirmed participants will be issued an invitation letter, to be used in the visa application process.</p>
		<p class="home-preview">Attendees are recommended to check the full requirements for a visa application, allowing plenty of time to provide the necessary documentation forrom the institution the application is being processed through.</p>
		<p class="home-preview">Information provided and visa issuance is beyond the organizers PCHRD's control. The organizers PCHRD are not liable for visa processing and issuance / outcome.</p>
		<p class="home-preview">The organizers will not refund registration fee in case of applicant's failure to obtain a visa. </p>
		<h3 class="font-roboto">Travel Grants</h3>
		<p class="home-preview">The Forum allocates a limited number of slots for travel grants. As soon as you get your abstract approved for presentation, you may inquire at <a href="mailto:travelgrants@forum2015.org" target="_blank">travelgrants@forum2015.org</a>.</p>
	</div>
@stop