@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">SPONSORS</h1>
  	<div class="page-content center">
  		<br/>
        <a href="http://www.picc.gov.ph/" target="_blank"><img src="images/sponsors/picc.png" /></a><br/>
        <a href="http://www.healthresearch.ph/" target="_blank"><img src="images/sponsors/pnhrs-small-logo2.png" /></a><br/>
        <h3 class="font-roboto">Headquarter Hotel</h3>
        <a href="http://www.sofitel.com/" target="_blank"><img src="images/sponsors/logo-sofitel.png" /></a><br/>
	</div>
	<br/>
@stop