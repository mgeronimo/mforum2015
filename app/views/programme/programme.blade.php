@extends('layouts.basic')

@section('content')
  	<h1 class="font-roboto center page-title">PROGRAMME</h1>
  	<div class="page-content">
  		<!--This is just a sample content, just for the sake of this page having a content-->
        <p>The Global Forum on Research and Innovation for Health 2015 aims to identify solutions to the world’s unmet health needs through research and innovation.</p>
        <p>As world leaders shift their focus to the post-2015 Sustainable Development Goals, increased understanding of the impact of research and innovation on health is of critical importance. Scientific research and innovation has transformative effects on not only health outcomes, but also on national economic growth and sustainable development. Forum 2015 provides a platform where low and middle income countries take prime position in defining the global health research agenda, in presenting solutions and in creating effective partnerships for action.</p>
        <p>The Council on Health Research for Development (COHRED), in partnership with the Philippine Department of Health and Philippine Department of Science and Technology, will host the Global Forum on Research and Innovation for Health in Manila, from 24-27 August 2015. Forum 2015 will bring together all stakeholders who play a role in making research and innovation benefit health, equity and development. This includes high-level representatives from government, business, non-profits, international organizations, academic and research institutions and social entrepreneurs among others.</p>
        <p>The Global Forum for Research and Innovation is the successor to the Global Forum for Health Research, last held in Cape Town, South Africa in 2012.</p>
	</div>
	<br/><br/>
@stop