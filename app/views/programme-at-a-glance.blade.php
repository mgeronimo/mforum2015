@extends('layouts.basic')

@section('content')
	<h1 class="font-roboto center page-title">PROGRAMME AT A GLANCE</h1>
	<div class="page-content">
      <h3 class="font-roboto">Programme pillars and themes</h3>
      <ol class="home-preview">
        <li>Increasing the Effectiveness of Research and Innovation for Health (through):
          <ul>
            <li>Social Accountability</li>
            <li>Increasing investments</li>
            <li>Country-driven capacity building</li>
          </ul>
        </li>
        <li>The role of research and innovation (in improving):
          <ul>
            <li>Food safety and security</li>
            <li>Health in mega-cities</li>
            <li>Disaster risk reduction</li>
          </ul>
        </li>
      </ol>
      <p>Over the course of three days, Forum 2015 will use informative and interactive discussions, workshops, networking sessions and activities to allow participants to interact, inspire, learn and partner to increase their own impact.</p>
      <p>The programme for this event will be built around two major pillars showcasing: (1) key concepts needed to improve the efficiency and effectiveness of research and innovation for health and development, as well as (2) ways that research and innovation contribute to solutions to important global health and development challenges faced by low and middle income countries today.</p>
      <p>In addition, important cross-cutting issues related to big data, m-Health and e-Health, youth initiatives and regional research priorities will appear throughout the programme.</p>
	</div>
	<br/>
  <h1 class="font-roboto center page-title">Program Outline as of October 14, 2014</h1>
  <div class="page-content">
    <div class="mart30">
      <h3 class="font-roboto">August 24 (Monday)</h3>
      <ul class="home-preview">
        <li><span class="time">8:30-15:30</span> ---- <em>Free Time</em></li>
        <li><span class="time">15:30-17:00</span> -- Opening Ceremony</li>
        <li><span class="time">17:00-19:00</span> -- Welcome Reception</li>
      </ul>
    </div>
    <div class="mart30">
      <h3 class="font-roboto">August 25 (Tuesday)</h3>
      <ul class="home-preview">
        <li><span class="time">8:30-10:00</span> ---- Plenary Session</li>
        <li><span class="time">10:00-10:30</span> -- <em>Coffee Break</em></li>
        <li><span class="time">10:30-12:00</span> -- Parallel Sessions</li>
        <li><span class="time">12:00-13:30</span> -- <em>Lunch Break</em></li>
        <li><span class="time">13:30-15:00</span> -- Parallel Sessions</li>
        <li><span class="time">15:00-15:30</span> -- <em>Coffee Break</em></li>
        <li><span class="time">15:30-17:00</span> -- Parallel Sessions</li>
        <li><span class="time">17:30-19:30</span> -- Networking Cocktail</li>
        <li class="market"><strong>8:30-17:00</strong> -- Market Place</li>
      </ul>
    </div>
    <div class="mart30">
      <h3 class="font-roboto">August 26 (Wednesday)</h3>
      <ul class="home-preview">
        <li><span class="time">8:30-10:00</span> ---- Plenary Session</li>
        <li><span class="time">10:00-10:30</span> -- <em>Coffee Break</em></li>
        <li><span class="time">10:30-12:00</span> -- Parallel Sessions</li>
        <li><span class="time">12:00-13:30</span> -- <em>Lunch Break</em></li>
        <li><span class="time">13:30-15:00</span> -- Parallel Sessions</li>
        <li><span class="time">15:00-15:30</span> -- <em>Coffee Break</em></li>
        <li><span class="time">15:30-17:00</span> -- Parallel Sessions</li>
        <li><span class="time">19:30-23:00</span> -- Philippine Night</li>
        <li class="market"><strong>8:30-17:00</strong> -- Market Place</li>
      </ul>
    </div>
    <div class="mart30">
      <h3 class="font-roboto">August 27 (Wednesday)</h3>
      <ul class="home-preview">
        <li><span class="time">8:30-10:00</span> ---- Plenary Session</li>
        <li><span class="time">10:00-10:30</span> -- <em>Coffee Break</em></li>
        <li><span class="time">10:30-12:00</span> -- Closing Plenary</li>
        <li><span class="time">12:00-17:00</span> -- <em>Free Time</em></li>
        <li class="market"><strong>8:30-12:00</strong> -- Market Place</li>
      </ul>
    </div>
    <p id="reference" style="margin: 30px 0;"><strong>NOTE: </strong>Registration on August 24, 25 and 26 will go on the whole day.</p>
    <p>Below is an illustration of the Program Outline for better viewing:</p>
    <img src="images/program_outline.png"  />
  </div>
@stop