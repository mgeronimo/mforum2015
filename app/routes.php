<?php

Route::get('/', function()
{
	return View::make('home');
});


/**********HOME BUTTON PAGES**********/
Route::get('/why-attend-forum-2015', function()
{
	return View::make('why-attend-forum-2015');
});
Route::get('/important-dates', function()
{
	return View::make('important-dates');
});
Route::get('/programme-at-a-glance', function()
{
	return View::make('programme-at-a-glance');
});
Route::get('/about-forum-2015', function()
{
	return View::make('about-forum-2015');
});
Route::get('/sponsors', function()
{
	return View::make('sponsors');
});

/**********SIDE MENU**********/
Route::get('/about-2015', function()
{
	return View::make('about-2015.about-2015');
});
Route::get('/get-involved', function()
{
	return View::make('get-involved.get-involved');
});
Route::get('/programme', function()
{
	return View::make('programme.programme');
});
Route::get('/practical-information', function()
{
	return View::make('practical-information.practical-information');
});
Route::get('/news', function()
{
	return View::make('news');
});
Route::get('/downloads', function()
{
	return View::make('downloads');
});
Route::get('/contact-us', function()
{
	return View::make('contact-us');
});

/**********ABOUT 2015 SUBMENUS**********/
Route::get('/about-2015/background-and-objectives', function()
{
	return View::make('about-2015.background-and-objectives');
});
Route::get('/about-2015/cohred', function()
{
	return View::make('about-2015.cohred');
});
Route::get('/about-2015/dost', function()
{
	return View::make('about-2015.dost');
});
Route::get('/about-2015/doh', function()
{
	return View::make('about-2015.doh');
});
Route::get('/about-2015/pchrd', function()
{
	return View::make('about-2015.pchrd');
});
Route::get('/about-2015/health-research-in-the-phil', function()
{
	return View::make('about-2015.health-research-in-the-phil');
});
Route::get('/contact-us', function()
{
	return View::make('contact-us');
});

/**********GET INVOLVED SUBMENUS**********/
Route::get('/get-involved/register', function()
{
	return View::make('get-involved.register');
});
Route::get('/get-involved/submit-an-abstract', function()
{
	return View::make('get-involved.submit-an-abstract');
});
Route::get('/get-involved/sponsorship-and-marketplace', function()
{
	return View::make('get-involved.sponsorship-and-marketplace');
});


/**********DOWNLOAD**********/
Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = storage_path() .'/file/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9\-\_\.]+');


/**********PRACTICAL INFOS SUBMENUS**********/
Route::get('/practical-information/about-manila-and-philippines', function()
{
	return View::make('practical-information.about-manila-and-philippines');
});
Route::get('/practical-information/accomodation', function()
{
	return View::make('practical-information.accomodation-new');
});
Route::get('/practical-information/travel-visa', function()
{
	return View::make('practical-information.travel-visa');
});
Route::get('/practical-information/picc-venue', function()
{
	return View::make('practical-information.picc-venue');
});
Route::get('/practical-information/tours-and-excursion', function()
{
	return View::make('practical-information.tours-and-excursion');
});
Route::get('/practical-information/faq', function()
{
	return View::make('practical-information.faq');
});